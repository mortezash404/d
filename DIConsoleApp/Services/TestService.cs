﻿using System;

namespace DIConsoleApp.Services
{
    public class TestService : ITestService
    {
        public void SayHello()
        {
            Console.WriteLine("this is sample of microsoft dependency injection");
        }
    }
}
