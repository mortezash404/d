﻿using DIConsoleApp.Services;
using Microsoft.Extensions.DependencyInjection;

namespace DIConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var provider = IoCManager.ConfigureServices();

            provider.GetService<TestService>().SayHello();
        }
    }
}
