﻿using DIConsoleApp.Services;
using Microsoft.Extensions.DependencyInjection;

namespace DIConsoleApp
{
    public static class IoCManager
    {
        public static ServiceProvider ConfigureServices()
        {
            return new ServiceCollection()
                .AddSingleton<TestService>()
                .AddTransient<ITestService, TestService>()
                .BuildServiceProvider();
        }
    }
}
